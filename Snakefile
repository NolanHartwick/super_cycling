import json
import os
from snakemake.utils import validate


validate(config, "schema/config.schema.json")


rule make_cycling_conf:
    input:
        config["counts"]
    output:
        "simpler_cycling.config.json"
    params:
        opts = config["cycling_opts"]
    run:
        d = {
            "counts": os.path.join("../", input[0]),
            **params["opts"]
        }
        with open(output[0], "w") as fout:
            json.dump(d, fout, indent=4)


rule identify_cyclers:
    input:
        counts=config["counts"],
        conf=rules.make_cycling_conf.output[0]
    output:
        "cycling/cycling_results.tsv"
    params:
        outdir = "cycling",
        snake_opts = config["snake_opts"],
        snakefile = os.path.join(workflow.basedir, "subworkflows", "simpler_cycling", "Snakefile")
    threads:
        8
    shell:
        """
        snakemake -d {params.outdir} --configfile {input.conf} \\
            -j {threads} --snakefile {params.snakefile} \\
            {params.snake_opts}
        """


rule generate_gene_lists:
    input:
        rules.identify_cyclers.output[0],
        config["promoters"]
    output:
        directory("gene_lists"),
        "salk_element.config.json"
    params:
        element_opts = config["element_opts"]
    script:
        "scripts/generate_lists.py"


rule do_element:
    input:
        gene_lists = rules.generate_gene_lists.output[0],
        conf = rules.generate_gene_lists.output[1]
    output:
        "element/element_summary.html"
    params:
        outdir = "element",
        snakefile = os.path.join(workflow.basedir, "subworkflows", "salk_element", "Snakefile"),
        snake_opts = config["snake_opts"]
    threads:
        8
    shell:
        """
        snakemake -d {params.outdir} --configfile {input.conf} \\
            -j {threads} --snakefile {params.snakefile} \\
            {params.snake_opts}
        """

