import pandas
import json
import os


def main(cyc_results, promoters, outdir, outconf):
    os.makedirs(outdir, exist_ok=True)
    cyc_results = pandas.read_csv(cyc_results, sep="\t", index_col=0)
    cyclers = cyc_results[cyc_results.haystack_FDR < 0.05]
    lags = list(sorted(cyclers.haystack_lag.unique()))
    lags_to_lists = {l: [] for l in lags}
    for gene, lag in zip(cyclers.index, cyclers.haystack_lag):
        lags_to_lists[lag].append(gene) 
    lags_to_path = {
        lag: os.path.join(outdir, f'cyclers_L{lag}.txt')
        for lag in lags
    }
    for lag in lags:
        with open(lags_to_path[lag], "w") as fout:
            fout.write("\n".join(lags_to_lists[lag]))
    lags_to_path = {
        lag: os.path.join("../", p)
        for lag, p in lags_to_path.items()
    }
    promoters = os.path.join("../", promoters)
    conf = {"jobs": {
        f"cyc_l{lag}": {"genes": lags_to_path[lag], "promoters": promoters}
        for lag in lags
    }}
    with open(outconf, "w") as fout:
        json.dump(conf, fout, indent=4)


def snakemain():
    cyc_results, promoters = snakemake.input
    outdir, outconf = snakemake.output
    main(cyc_results, promoters, outdir, outconf)


if(__name__ == "__main__"):
    snakemain()
